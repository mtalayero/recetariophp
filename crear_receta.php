<?php
include 'lib/funciones.php';
$receta = array();
	$nombre = $_POST["nombre"]; 
	$tipo = $_POST["tipo"];
	$duracion = $_POST["duracion"];
	$elaboracion =$_POST["preparacion"];
	$usuario = $_SESSION["id"];
	$descripcion = $_POST['descripcion'];
	$hoy = date("Y-m-d");
	$creacion= $hoy;
	$edicion = $hoy;
	

/* 	if ($_FILES["imagen"]["name"] != ""){
                // Ruta relativa carpeta donde quieres guardar la imagen
		$target_path = "img/imgdb/";
		$target_path = $target_path . "receta" . "-" . $_FILES['imagen']['name'];
                // Copiar la imagen a la ruta y con el nombre que hemos indicado
		copy($_FILES['imagen']['tmp_name'], $target_path);
                // Guardo la ruta de la imagen para después insertarla en la BD
		$imagen = $target_path;
	}else{
		$imagen =null;
	}*/
	try {
		$conn = new PDO('sqlite:recetario.db');
		$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$insertar ="INSERT INTO recetas_receta (nombre, descripcion,tipo, foto, elaboracion, tiempo_elaboracion, fecha_creacion, fecha_ultima_edicion, usuario_id, votos)
		 VALUES (:nombre, :descripcion, :tipo,'imgdb/Captura_de_pantalla_de_2013-12-02_131207_1.png' ,:elaboracion, :tiempo, :creacion, :edicion, :usuario, 0)";
		$sentencia = $conn->prepare($insertar);

		$sentencia -> bindParam(':nombre',$nombre );
	    $sentencia -> bindParam(':descripcion',  $descripcion);
	    $sentencia -> bindParam(':tipo',  $tipo);
	    $sentencia -> bindParam(':elaboracion', $elaboracion );
	    $sentencia -> bindParam(':tiempo', $duracion );
	    $sentencia -> bindParam(':usuario', $usuario );
	    $sentencia -> bindParam(':creacion', $creacion);
	    $sentencia -> bindParam(':edicion',$edicion );
	    $ok = $sentencia -> execute();
		$conn=null;
		if(!empty($_POST["cantidad"]) && !empty($_POST["ingrediente"])){

		$arrCantidad = $_POST["cantidad"];
		$arrIngrediente = $_POST["ingrediente"];
	
		//sacamos el id de la receta recien insertada
		$conn = new PDO('sqlite:recetario.db');
		$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$consulta ="SELECT id FROM recetas_receta WHERE nombre=:nombre AND descripcion=:descripcion;";
		$sen_buscaid=$conn->prepare($consulta);
		$sen_buscaid->bindParam(":descripcion", $descripcion);
		$sen_buscaid->bindParam(":nombre", $nombre);
		$sen_buscaid->execute();
		$ids = $sen_buscaid->fetchAll(PDO::FETCH_ASSOC);
	
		foreach ($ids as $id){
			
			 $receta = $id["id"];
		}
	
		$conn=null;
		guardar_ingredientes_cantidades($receta, $arrCantidad, $arrIngrediente);
	}
		
	} catch(PDOException $e ){
			echo $e -> getMessage();
	}  
 	header("Location: perfil.php");
	
	



?>