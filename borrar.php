<?
session_start();
if ($_SESSION["id"]){
	$receta = $_GET['id'];
	//comprobamos que el usuario que quiere borrar la receta es el dueño
	try{
		    $consulta = "SELECT usuario_id FROM recetas_receta WHERE id = :receta and usuario_id =:usuario";
			$usuario=$_SESSION["id"];
			$conn = new PDO('sqlite:recetario.db'); 		
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		
			$sentencia = $conn->prepare($consulta);
	
			$sentencia->bindParam(":receta", $receta);
			$sentencia->bindParam(":usuario", $usuario);
	
			$sentencia-> execute();
			$rows =$sentencia->fetch(PDO::FETCH_ASSOC);
			$conn = null;
             //si la receta es del usuario que la intenta borrar
             
        	 if (count($rows)>0) {
        	 	
        	 	try{
	        	 	$conn = new PDO('sqlite:recetario.db'); 		
				    $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			
	        		$borrar="DELETE FROM recetas_receta WHERE id=:id";
			
					$sentencia_borrar = $conn->prepare($borrar);
					$sentencia_borrar->bindParam(":id", $receta);
			
					$sentencia_borrar -> execute();
					$conn = null;
					
					header( 'Location: perfil.php');
				}catch(PDOException $e ){
			 			
			 		echo  $e->getMessage();
				}
			}
			
			 			

	}catch(PDOException $e ){
			 echo $e->getMessage();
	}
}
	

?>