<?php

	error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
include 'config.php';
include_once "PasswordHash.php";
function clean($dato){
	return htmlentities(strip_tags(trim($dato)));
}
function session_inciada(){
	if (isset($_SESSION["usuario"])){
		$usuario=$_SESSION["usuario"];
		return $usuario;
	}
}
function obtener_id($receta){
		try{
			$conn = new PDO('sqlite:recetario.db');
			$busca_id=$conn->prepare( "SELECT id FROM recetas_receta WHERE nombre=:nombre AND descripcion= :descripcion;");
			$busca_id -> bindParam('nombre', $receta['nombre']);
			$busca_id -> bindParam('descripcion', $receta['descripcion']);
			$busca_id->execute($recetad);
			foreach($busca_id as $id){
				$id_receta = $id['id'];
			}
		}catch(PDOException $e ){
			echo $e -> getMessage();
		}			
		$conn = null;
		return (integer)($id_receta);
}

function obtener_ultimas_recetas(){
	try{
			$conn = new PDO('sqlite:recetario.db'); 
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			
			$consulta = "SELECT recetas_receta.*, username FROM recetas_receta, auth_user WHERE recetas_receta.usuario_id = auth_user.id";	
			
			$resultado = $conn -> query($consulta);   
			
         	$resultado->setFetchMode(PDO::FETCH_ASSOC);
			$ultimas_recetas = $resultado->fetchall();
		

			$conn = null;

	}catch(PDOException $e ){
			 $ultimas_recetas =  $e->getMessage();
			 
	}
	return $ultimas_recetas ;
};
function obtener_recetas_usuario($id){
		try{
			$conn = new PDO('sqlite:recetario.db'); 
			
			$consulta = "SELECT recetas_receta.*, username FROM recetas_receta, auth_user WHERE recetas_receta.usuario_id = auth_user.id AND recetas_receta.usuario_id = :id";	
			$sentencia = $conn ->prepare($consulta);
			$sentencia ->bindParam(":id", $id );
			$sentencia ->execute();
			
         	$recetas_usuario = $sentencia->fetchAll(PDO::FETCH_ASSOC);


			$conn = null;

	}catch(PDOException $e ){
			 $recetas_usuario =  $e->getMessage();
			 
	}
	return $recetas_usuario;
	
};
function comprobar_ingrediente($ingrediente){
	$res ="se qeda";
	try{
		
			$conn = new PDO('sqlite:recetario.db'); 
			$res = "conecta";
			$ingrediente = ucfirst($ingrediente);
			$consulta = "SELECT ingrediente FROM recetas_ingrediente WHERE recetas_ingrediente.ingrediente = :ingrediente";	
			$sentencia = $conn ->prepare($consulta);
			$sentencia ->bindParam(":ingrediente", $ingrediente );
			$sentencia ->execute();
			$res = "execute";
			if(count($sentencia->fetchAll())==0){
					$conn = null;
				    $res =true;
 				//guardamos ingrediente
			}else{
					$conn = null;
				    $res =false;//no guardamos ingrediente
			}

	}catch(PDOException $e ){
			$res = $e->getMessage();
			 
	}
	return $res;
	
};
function guardar_ingrediente($ingrediente){
	$resultado = comprobar_ingrediente($ingrediente);
	$ingrediente = ucfirst($ingrediente);
	if(comprobar_ingrediente($ingrediente)){
		try{
			$conn = new PDO('sqlite:recetario.db'); 		
			
			$sentencia = $conn->prepare("INSERT INTO recetas_ingrediente VALUES(null, :ingrediente)");
			
			//$resultado = "sentencia preparaa";
 			  $sentencia=$conn->prepare("INSERT INTO recetas_ingrediente VALUES(null, :ingrediente)");
			  $sentencia -> bindParam(':ingrediente', $ingrediente);
			 $ok=$sentencia->execute();
			 if (!$ok) die ("No se puede ejecutar sentencia");
			$conn = null;
	
		}catch(PDOException $e ){
				 $resultado = $e->getMessage();
		}
	}
	return $resultado;
}
function guardar_ingredientes_cantidades($idreceta, $arrCantidad, $arrIngrediente){
	$conn = new PDO('sqlite:recetario.db');
	 $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	for ($i = 0; $i<count($arrCantidad); $i++){
		$ingrediente = $arrIngrediente[$i];
		if($arrCantidad[$i]!=""){

			try{
	
				$buscar_ingrediente ="SELECT id FROM recetas_ingrediente WHERE ingrediente=:ingrediente;";
				
				$sen_buscaid=$conn->prepare($buscar_ingrediente);
				$sen_buscaid->bindParam(":ingrediente", $ingrediente);
				$sen_buscaid->execute();
				if($sen_buscaid->fetchColumn()==0){
	 				 guardar_ingrediente($ingrediente);
				}
				//Saco el id.
				$sen_buscaid=$conn->prepare($buscar_ingrediente);
				$sen_buscaid->bindParam(":ingrediente", $ingrediente);
				$sen_buscaid->execute();
				
				foreach($sen_buscaid as $ingid){
					$idingrediente=$ingid["id"];
				}				//Introduzco las cantidades que se corresponden con el ingrediente
				
				$sen_guardarcantidad=$conn->prepare("INSERT INTO recetas_recetaingrediente (receta_id,ingrediente_id,cantidad) VALUES(:receta, :ingrediente, :cantidad);");
				$sen_guardarcantidad->bindParam(":receta", $idreceta);
				$sen_guardarcantidad->bindParam(":ingrediente", $idingrediente);
				$sen_guardarcantidad->bindParam(":cantidad", $arrCantidad[$i]);
				
				$sen_guardarcantidad->execute();
				
		    }catch(PDOException $e ){
					 $resultado = $e->getMessage();
			}
		}
		
	}
}
/*function guardar_receta($receta){
         
	
}*/
?>