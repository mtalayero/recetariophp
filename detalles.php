<?php
include 'lib/config.php';

$template = $twig -> loadTemplate("recetas/receta.html");
	try{
			$id = $_GET['id'];
			//Buscar la receta solicitada
			$conn = new PDO('sqlite:recetario.db'); 
			/*$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );*/
			
			$consulta = "SELECT *, auth_user.username FROM recetas_receta, auth_user WHERE recetas_receta.usuario_id = auth_user.id AND recetas_receta.id = :id";	
			$sentencia = $conn -> prepare($consulta);
			$sentencia -> bindParam(':id', $id );
			
			$sentencia->execute(); 
			$receta = $sentencia->fetch(PDO::FETCH_ASSOC);
			$conn = null;
			
			//Buscar ingredientes relacionados	
			$conn = new PDO('sqlite:recetario.db'); 
		
			$consulta = "SELECT recetas_ingrediente.ingrediente, recetas_recetaingrediente.cantidad FROM recetas_receta, recetas_recetaingrediente, recetas_ingrediente  WHERE recetas_receta.id = recetas_recetaingrediente.receta_id AND recetas_recetaingrediente.ingrediente_id = recetas_ingrediente.id AND recetas_recetaingrediente.receta_id = :id";
			$sentencia = $conn -> prepare($consulta);
			$sentencia -> bindParam(':id', $id );
			
			$sentencia->execute(); 
			$ingredientes = $sentencia->fetchAll(PDO::FETCH_ASSOC);
			$conn = null;
			
	}catch(PDOException $e ){
			 $receta =  $e->getMessage();
	}

$datos = array(
		'receta' => $receta,
		'ingredientes' => $ingredientes
		);

echo $template -> render($datos);

?>
