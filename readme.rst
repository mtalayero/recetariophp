RECETARIO
====================

Aplicaciónn para gestionar, ver y valorar recetas propias y las proporcionadas por otros usuarios.
DESCRIPCION
=============

AplicaciÓn para gestionar, ver y valorar recetas propias y las proporcionadas por otros usuarios.


INSTALACION
===================

instalacion de vendor, mediante el fichero composer.json proporcionado.
Nos situamos en el directorio raíz

::
	curl -s http://getcomposer.org/installer | php
	
Instalamos composer.json
::
	 php composer.phar install
Subiremos todos los archivos a nuestro hosting, y ya está.


USO
====================
Esta aplicación la pueden usar tanto los usuarios que se registran, como los que prefieren no hacerlo.

Todos los usuarios tienen una página principal dónde aparece un listado con todas las recetas, mostrando primero las últimas.
AparecerÃ¡ un panel a la derecha, donde podrÃ¡n acceder a los platos mejor valorados de cada categorÃ­a.

Se puede buscar ademÃ¡s con el buscador que hay siempre en la cabecera, buscara por nombre de receta e ingredientes,
ademÃ¡s con las casillas podemos seleccionar a que categorí­a pertenece lo que estamos buscando.

Seleccionando el nombre de usuario que aparece cuando se muestran los detalles de la receta, veremos todas las recetas que ha creado ese usuario.

Los usuarios que se registran pueden crear recetas, pudiendo borrarlas o modificarlas cuando quieran.
Pueden votar  las recetas existentes o retirar su voto si ya habian votado, cada usuario solo puede votar una vez una receta.


