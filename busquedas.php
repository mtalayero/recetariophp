<?php
include 'lib/funciones.php';

$template = $twig -> loadTemplate("recetas/resultados.html");
	if(empty($_GET['plato']) and empty($_GET['buscar'])){
			$filtro=" No podemos ayuarle si no nos dice que quiere buscar";
		
	}else{
		try{
				$conn = new PDO('sqlite:recetario.db'); 
				$filtro ="";
				$busc = FALSE;
				$plat = FALSE;
				$buscar ="";
				$tipos=array();
				
				//Preparar la consulta y despues modificar segun lo que queramos buscar
				$buscar_recetas = "SELECT recetas_receta.*, auth_user.username 
							FROM recetas_receta, auth_user 
							WHERE recetas_receta.usuario_id = auth_user.id ";
				
				/*Comprobamos en las dos variables si llegan datos o no.
				  si llegan datos:
				 * marcamos la booleana correspondiente a true;
				 * asignamos el valor al filtro para mostrarlo despues
				 * Guardamos el valor en la variable con los comodines para usarla como bindparam
				 * Concatenamos la consulta a la parte ya creada
				 * */
				
				if (!empty($_GET['buscar'])){
					$busc = TRUE;
					$filtro = $filtro. " ". $_GET['buscar']. " | ";
					$buscar = '%'. $_GET['buscar'].'%';
					$buscar_recetas = $buscar_recetas." AND (nombre LIKE :buscar OR descripcion LIKE :buscar) ";				
				}
			
				if (!empty($_GET['plato'])){ 	
					$plat = TRUE;
					$buscar_recetas = $buscar_recetas . ' AND (';
					$i=0;
					foreach($_GET['plato'] as $tipo){
						if ($i==0){
							$buscar_recetas = $buscar_recetas . " recetas_receta.tipo = :tipo".$i;
						}else{
							$buscar_recetas = $buscar_recetas . " OR recetas_receta.tipo = :tipo".$i;
						}
						$i += 1;
						array_push($tipos, $tipo);
						
						$filtro = $filtro. " ". $tipo . " | ";	
					}
					$buscar_recetas = $buscar_recetas . ')';
				}
				//Preparamos la consulta
				$sentencia_buscar_recetas = $conn -> prepare($buscar_recetas);
				
				//Estableceremos los parametros segun los datos introducidos, lo sabremos gracias a las boleanas
				if ($busc){
					$sentencia_buscar_recetas -> bindParam(':buscar', $buscar );
				}
				if($plat){
					$i=0;
					foreach($tipos as $tipo){
						$rem = ':tipo'.$i;
						$sentencia_buscar_recetas -> bindParam($rem, $tipo );
						$i += 1;
					}
				}			
				//Ejecutamos la consulta y las guardamos en un array
				$sentencia_buscar_recetas->execute(); 
		//		echo $buscar_recetas;
				$arrRecetas = $sentencia_buscar_recetas->fetchAll(PDO::FETCH_ASSOC);
				$conn = null;
				
				//Necesitamos también los ingredientes relacionados en caso de que venga algo en el name "buscar"
				/*
				 * ESTRUCTURA QUE NECESITAMOS CONSEGUIR
				 * Ingredientes ["ingrediente"] = [receta1,
				 * 								   receta2,
				 * 								   receta3]
				 * */
				
				$buscar_ingredientes = "SELECT ingrediente FROM recetas_receta, recetas_ingrediente, recetas_recetaingrediente
									WHERE recetas_receta.id = recetas_recetaingrediente.receta_id 
									AND recetas_ingrediente.id = recetas_recetaingrediente.ingrediente_id ";
				//añadir filtros a la consulta segun necesidades
				if($busc){
					$buscar_ingredientes = $buscar_ingredientes." AND ingrediente LIKE :buscar ";			
				}
					
				if($plat){
						$buscar_ingredientes = $buscar_ingredientes . ' AND (';
						$i=0;
						foreach($tipos as $tipo){
							if ($i==0){
								$buscar_ingredientes = $buscar_ingredientes . " recetas_receta.tipo = :tipo".$i;
							}else{
								$buscar_ingredientes = $buscar_ingredientes . " OR recetas_receta.tipo = :tipo".$i;
							}
							$i += 1;
						}
						$buscar_ingredientes = $buscar_ingredientes . ')';
					}
					$conn = new PDO('sqlite:recetario.db');
					//preparar la consulta
					$sentencia_buscar_ingredientes = $conn -> prepare($buscar_ingredientes);
					//indeicar los parametros de lo que necesitamos
					if($busc){
						$sentencia_buscar_ingredientes -> bindParam(':buscar', $buscar );
					}
					if($plat){
						$i=0;
						foreach($tipos as $tipo){
							$sentencia_buscar_ingredientes -> bindParam(':tipo'.$i, $tipo );
							$i += 1;
						}
					}
					$sentencia_buscar_ingredientes->execute(); 
					//aqui recogemos resultado de ingredientes   fetchall guardatodos pero no muestra
														//		 fetch muestra pero solo uno
					$arrIngredientes = $sentencia_buscar_ingredientes->fetchAll(PDO::FETCH_ASSOC);
					$conn = null;
			
					//buscar recetas asociadas en los ingredientes
					$arrIngredientes_recetas = array();
					foreach ($arrIngredientes as $ingrediente){
						$buscar_recetas_ingredientes = "SELECT recetas_receta.nombre, recetas_receta.id FROM recetas_receta, recetas_ingrediente, recetas_recetaingrediente
									WHERE recetas_receta.id = recetas_recetaingrediente.receta_id 
									AND recetas_ingrediente.id = recetas_recetaingrediente.ingrediente_id  
									AND (recetas_ingrediente.ingrediente = :ingrediente OR recetas_ingrediente.ingrediente LIKE :ingrediente2)";
						
						if($plat){
							$buscar_recetas_ingredientes = $buscar_recetas_ingredientes . 'AND (';
							$i=0;
							foreach($tipos as $tipo){
								if ($i==0){
									$buscar_recetas_ingredientes = $buscar_recetas_ingredientes . " recetas_receta.tipo = :tipo".$i;
								}else{
									$buscar_recetas_ingredientes = $buscar_recetas_ingredientes . " OR recetas_receta.tipo = :tipo".$i;
								}
								$i += 1;
							}
							$buscar_recetas_ingredientes = $buscar_recetas_ingredientes . ')';
						}	
						$arrRec=array();	
						$conn = new PDO('sqlite:recetario.db'); 
								$sentencia_recetas_ingredientes= $conn ->prepare($buscar_recetas_ingredientes);
						$ingrediente2 = '%'.$ingrediente["ingrediente"].'%';
						$sentencia_recetas_ingredientes -> bindParam(':ingrediente', $ingrediente["ingrediente"]);
						$sentencia_recetas_ingredientes -> bindParam(':ingrediente2', $ingrediente2);
						
						if($plat){
							$i=0;
							foreach($tipos as $tipo){
								$sentencia_recetas_ingredientes -> bindParam(':tipo'.$i, $tipo );
								$i += 1;
							}
						}
							//Ejecutamos la consulta y las guardamos en un array
						$sentencia_recetas_ingredientes->execute(); 
						$arrRec = $sentencia_recetas_ingredientes->fetchAll(PDO::FETCH_ASSOC);
						$arrIngredientes_recetas[$ingrediente["ingrediente"]] =$arrRec;
										
						$conn = null;
					}
				
			
			}catch(PDOException $e ){
				echo  $e->getMessage();
			}
		}

$datos = array(
		'recetas' => $arrRecetas,
		'ingredientes' => $arrIngredientes_recetas,
		'filtro' => $filtro	,
		'usuario' => session_inciada()
);
echo $template -> render($datos);

?>
